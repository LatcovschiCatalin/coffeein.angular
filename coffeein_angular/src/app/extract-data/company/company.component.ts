import {Component, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';
import {CookieService} from "ngx-cookie-service";


@Component({
  selector: 'app-company',
  templateUrl: './company.component.html',
  styleUrls: ['./company.component.scss']
})

export class CompanyComponent implements OnInit {


  products;
  nr_products;
  id;
  n;
  lg;
  search_value = '';
  slug = 'all';
  color;
  isTop;
  isShow;
  value_language;
  whole_price;
  products_length;
  categories;
  count = 0;
  company_logo;
  toAdd = false;
  company_slug;
  products_cart;
  language;

  constructor(private http: HttpClient,
              private route: Router,
              private cookieService: CookieService,
  ) {
  }

  getSearchValue(value) {
    this.search_value = value;
    this.route.navigate(['/products', this.company_slug[2]], {queryParams: {searchTerm: value}});
    this.http.get<any>(`http://localhost:3000/api/products/sorted?page=&searchTerm=${this.search_value}&productCategory=all&company=${this.company_slug[2]}`).subscribe(data => {
      this.products = data.products;
      this.products_length = data.products.length;
      this.categories = data.categories;
    });
  }


  getWholePrice(price) {
    this.whole_price += Number(price);
    this.nr_products++;
    this.cookieService.set('totalPrice', String(this.whole_price))
    this.cookieService.set('countProducts', String(this.nr_products))
  }

  getProductCart(id) {
    for (let i = 0; i < this.products_cart.length; i++) {
      if (this.products_cart[i].id === id) {
        this.toAdd = true;
        this.products_cart[i].count++;
      }
    }
    if (this.toAdd) {

      this.toAdd = false;
    } else {
      this.products_cart[this.n] = {
        'id': id,
        'count': 1
      }
      this.n++;
    }

    this.count = 0;
    this.cookieService.delete('cart-products')

    this.cookieService.set('cart-products', JSON.stringify(this.products_cart))

  }

  hoverDescription(id) {
    this.isShow = true;
    this.id = id;
  }


  hideDescription() {
    this.isShow = false;
  }

  ngOnInit(): void {


    if (this.cookieService.get('lang') !== '') {
      this.language = this.cookieService.get('lang');
    } else {
      this.language = 'ro';
      this.cookieService.set('lang', 'ro');
    }
    if (this.cookieService.get('cart-products')) {
      this.n = JSON.parse(this.cookieService.get('cart-products')).length;
      this.products_cart = JSON.parse(this.cookieService.get('cart-products'));
    } else {
      this.n = 0;
      this.products_cart = [];
    }

    if (this.cookieService.get('totalPrice')) {
      this.whole_price = Number(this.cookieService.get('totalPrice'));
      this.nr_products = Number(this.cookieService.get('countProducts'));
    } else {
      this.whole_price = 0;
      this.nr_products = 0;
    }

    this.company_slug = this.route.url.split('/');
    this.http.get<any>(`http://localhost:3000/api/products/sorted?page=&searchTerm=${this.search_value}&productCategory=all&company=${this.company_slug[2]}`).subscribe(data => {
      this.products = data.products;
      this.products_length = data.products.length;
      this.categories = data.categories;
    });
    this.http.get<any>('http://localhost:3000/api/companies').subscribe(company => {
      for (let i = 0; i < company.docs.length; i++) {

        if (company.docs[i].slug === this.company_slug[2]) {
          this.company_logo = company.docs[i];
          this.color = company.docs[i].color;


        }
      }
    });
    this.http.get(`../assets/languages/${this.language}.json`).subscribe(data => {

      this.value_language = data;

    });

    this.http.get<any>('http://localhost:3000/api/languages').subscribe(data => {
      for (let i = 0; i < data.docs.length; i++) {
        if (data.docs[i].slug === this.language) {
          this.lg = i;
        }
      }
    });
  }


  selectCategory(slug) {

    if (slug !== '') {
      this.slug = slug;
      this.route.navigate(['/products', this.company_slug[2]], {queryParams: {productCategory: slug}});
    } else {
      this.slug = 'all';
      this.route.navigate(['/products', this.company_slug[2]], {
        queryParams: {productCategory: 'all'},
        queryParamsHandling: 'merge'
      });
    }
  }

}
