import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {HttpClient} from "@angular/common/http";
import {CookieService} from "ngx-cookie-service";

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit {
  carts;
  nr_products;
  totalPrice;
  counts = [];
  displays = [];
  z = 0;
  cards = [];
  language;
  lg;
  value_language;

  constructor(private router: Router,
              private http: HttpClient,
              public cookieService: CookieService) {
  }

  getDecrease(id, j, price) {
    for (let i = 0; i < this.carts.length; i++) {
      if (this.carts[i].id === id) {
        if (this.counts[j] > 0) {
          this.counts[j]--;
          this.carts[i].count--;
          this.nr_products--;
          this.totalPrice -= Number(price);
        }
      }
    }
    this.cookieService.set('cart-products', JSON.stringify(this.carts));
    this.cookieService.set('countProducts', String(this.nr_products));
    this.cookieService.set('totalPrice', String(this.totalPrice));

  }

  getIncrease(id, j, price) {
    for (let i = 0; i < this.carts.length; i++) {
      if (this.carts[i].id === id) {
        this.counts[j]++;
        this.carts[i].count++;
        this.nr_products++;
        this.totalPrice += Number(price);
      }
    }
    this.cookieService.set('cart-products', JSON.stringify(this.carts));
    this.cookieService.set('countProducts', String(this.nr_products));
    this.cookieService.set('totalPrice', String(this.totalPrice));


  }

  getCloseCart(id, j, price) {
    for (let i = 0; i < this.carts.length; i++) {
      if (this.carts[i].id === id) {
        this.totalPrice -= Number(price) * Number(this.carts[i].count);
        this.nr_products -= Number(this.carts[i].count);
        this.carts = this.carts.filter(el => el.id !== id)
        this.displays[j] = false;
      }
    }

    this.cookieService.set('cart-products', JSON.stringify(this.carts));
    this.cookieService.set('totalPrice', String(this.totalPrice));
    this.cookieService.set('countProducts', String(this.nr_products));


  }

  ngOnInit()
    :
    void {

    if (this.cookieService.get('lang') !== '' || null) {
      this.language = this.cookieService.get('lang');
    } else {
      this.language = 'ro';
      this.cookieService.set('lang', 'ro');
    }

    if (this.cookieService.get('totalPrice') !== '' || null) {
      this.totalPrice = Number(this.cookieService.get('totalPrice'));
      this.nr_products = Number(this.cookieService.get('countProducts'));
      this.carts = JSON.parse(this.cookieService.get('cart-products'));

    } else {
      this.totalPrice = 0;
      this.nr_products = 0;
      this.carts = '';
    }


    this.http.get<any>('http://localhost:3000/api/products/sorted').subscribe(product => {
      for (let i = 0; i < product.products.length; i++) {
        for (let a = 0; a < product.products[i].products.length; a++) {
          for (let j = 0; j < this.carts.length; j++) {
            if (product.products[i].products[a]._id === this.carts[j].id) {
              this.cards[this.z] = product.products[i].products[a];
              this.counts[this.z] = this.carts[j].count;
              this.z++;

            }
          }
        }
      }
    });
    this.http.get(`../assets/languages/${this.language}.json`).subscribe(data => {

      this.value_language = data;

    });

    this.http.get<any>('http://localhost:3000/api/languages').subscribe(data => {
      for (let i = 0; i < data.docs.length; i++) {
        if (data.docs[i].slug === this.language) {
          this.lg = i;
        }
      }
    });
  }

}
