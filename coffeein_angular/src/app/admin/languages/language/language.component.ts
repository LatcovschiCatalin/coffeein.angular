import {Component, OnInit, TemplateRef} from '@angular/core';
import {NbDialogService} from "@nebular/theme";
import {HttpClient} from "@angular/common/http";
import {AccessTokenService} from "../../consts/access-token.service";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-language',
  templateUrl: './language.component.html',
  styleUrls: ['./language.component.scss']
})
export class LanguageComponent implements OnInit {
  columns;
  response;
  id;
  isEditPage = false;

  object = {
    name: '',
    slug: '',
  };

  constructor(private dialogService: NbDialogService,
              private http: HttpClient,
              private accessToken: AccessTokenService,
              private route: ActivatedRoute,
              private router: Router) {

    route.queryParams.subscribe(p => {
      this.id = p.Id;
      if (this.id !== undefined) {
        this.isEditPage = true;
        this.http.get(`http://localhost:3000/api/languages/${this.id}`).subscribe(data => {
          // @ts-ignore
          this.object = data;
        });
      }
    });
  }

  open(dialog: TemplateRef<any>) {
    this.dialogService.open(dialog, {});
    this.router.navigate(
      ['.'],
      {relativeTo: this.route}
    );
    this.object = {
      name: '',
      slug: '',
    };
    this.isEditPage = false;
  }

  ngOnInit(): void {

    this.columns = {
      id: {
        title: 'ID'
      },
      name: {
        title: 'NAME'
      },
      slug: {
        title: 'SLUG'
      }
    }

    this.http.get('http://localhost:3000/api/languages?limit=9999999999999999999').subscribe(data => {
      // @ts-ignore
      this.response = data.docs;
    });


  }

  onSubmit() {


    if (this.isEditPage) {
      this.http.put(`http://localhost:3000/api/languages/${this.id}`, JSON.stringify(this.object), {headers: this.accessToken.headers}).subscribe(res => {
        this.http.get('http://localhost:3000/api/languages?limit=9999999999999999999').subscribe(data => {
          // @ts-ignore
          this.response = data.docs;
        });
      });
    } else {
      this.http.post(`http://localhost:3000/api/languages`, JSON.stringify(this.object), {headers: this.accessToken.headers}).subscribe(res => {
        this.http.get('http://localhost:3000/api/languages?limit=9999999999999999999').subscribe(data => {
          // @ts-ignore
          this.response = data.docs;
        });
      });
    }


  }
}
