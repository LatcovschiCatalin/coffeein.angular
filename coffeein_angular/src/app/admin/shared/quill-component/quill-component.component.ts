import {Component, forwardRef, Input, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {noop} from "rxjs";
import {NG_VALUE_ACCESSOR} from "@angular/forms";


@Component({
  selector: 'app-quill-component',
  templateUrl: './quill-component.component.html',
  styleUrls: ['./quill-component.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => QuillComponentComponent),
      multi: true
    }
  ]

})


export class QuillComponentComponent implements OnInit {
  maxUploadFileSize = 10000000;
  @Input() formControlName;
  @Input() placeholder;
  @Input() nullToEmptyString: boolean;
  quillEditorRef;
  value;
  disabled = false;
  quillModules;
  ql_editor1;
  attrQuillTxtArea;
  quillCustomDiv;
  editorTouched = false;
  initialValue;
  private onTouched: () => void = noop;
  private onChange: (_: any) => void = noop;
  @ViewChild('editor') editor;

  ngOnInit() {
    this.quillModules = {
      toolbar: [
        ['bold', 'italic', 'underline', 'strike'],

        [{ 'header': 2 }],
        [{ 'list': 'ordered' }, { 'list': 'bullet' }],

        [{ 'header': [1, 2, 3, 4, 5, 6, false] }],

        [{ 'color': new Array<any>() }, { 'background': new Array<any>() }],
        [{ 'align': new Array<any>() }],

        ['link', 'image', 'video'],
        ['code-block']
      ]

    };
  }

  async setEvents(customButton): Promise<void> {
    if (customButton) {
      customButton.addEventListener('click', () => {
        const wasActiveTxtArea_1 =
          (this.ql_editor1.getAttribute('quill__html').indexOf('-active-') > -1);
        if (wasActiveTxtArea_1) {

          this.quillEditorRef.clipboard.dangerouslyPasteHTML(this.ql_editor1.value);
          customButton.classList.remove('ql-active');
        } else {

          this.ql_editor1.value = this.quillEditorRef.root.innerHTML;
          customButton.classList.add('ql-active');
        }

        this.ql_editor1.addEventListener('change', () => {
          this.quillEditorRef.clipboard.dangerouslyPasteHTML(this.ql_editor1.value);
        });

        this.ql_editor1.setAttribute('quill__html', wasActiveTxtArea_1 ? '' : '-active-');
      });
    }
  }

  async getEditorInstance(editorInstance: any) {
    this.quillEditorRef = editorInstance;

    this.ql_editor1 = document.createElement('textarea');
    this.attrQuillTxtArea = document.createAttribute('quill__html');
    this.ql_editor1.setAttributeNode(this.attrQuillTxtArea);

    this.quillCustomDiv = this.quillEditorRef.addContainer('ql-custom');
    this.quillCustomDiv.appendChild(this.ql_editor1);

    const toolbar = editorInstance.getModule('toolbar');
    const buttonElement = toolbar.controls.filter(control => control[0] === 'code-block')[0];
    this.setEvents(buttonElement[1]);

    toolbar.addHandler('image', this.imageHandler);
  }

  imageHandler = (image, callback) => {
    const input = document.getElementById('fileInputField') as HTMLInputElement;
    document.getElementById('fileInputField').onchange = () => {
      let file: File;
      file = input.files[0];
      if (/^image\//.test(file.type)) {
        if (file.size > this.maxUploadFileSize) {
          alert('Image needs to be less than 1MB');
        } else {
        }
      } else {
        alert('You could only upload images.');
      }
    };

    input.click();
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  insertImageToEditor(res) {
    let url = res.filename;
    const range = this.quillEditorRef.getSelection();
    url = 'http://localhost:3000/api/public/articles/images/' + url;
    const img = '<img src="' + url + '" />';
    this.quillEditorRef.clipboard.dangerouslyPasteHTML(range.index, img);
  }

  updateValue(insideValue: any) {
    if (this.editorTouched) {
      const quillData = {
        html: ''
      };
      if (this.nullToEmptyString) {
        quillData.html = insideValue.html === null ? '' : insideValue.html;
      } else {
        quillData.html = insideValue.html;
      }
      this.value = quillData.html;
      this.onChange(this.value);
      this.onTouched();
    }
  }

  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;

  }

  writeValue(obj: any): void {
    this.initialValue = obj;
    setTimeout(
      () => {
        if (this.quillEditorRef) {
          this.quillEditorRef.root.innerHTML = this.initialValue;
        }
      }, 200
    );
  }

  onEditorTouched() {
    this.editorTouched = true;
  }

}
