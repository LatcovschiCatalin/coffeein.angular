import { NbMenuItem } from '@nebular/theme';

export const MENU_ITEMS: NbMenuItem[] = [
  {
    title: 'Companies',
    icon: '',
    link: '/admin/companies'
  },
  {
    title: 'Products',
    icon: '',
    link: '/admin/products'
  },
  {
    title: 'Product categories',
    icon: '',
    link: '/admin/product-categories'
  },
  {
    title: 'Slider info',
    icon: '',
    link: '/admin/slider-info'
  },
  {
    title: 'Users',
    icon: '',
    link: '/admin/users'
  },
  {
    title: 'Order requests',
    icon: '',
    link: '/admin/order-requests'
  },
  {
    title: 'Languages',
    icon: '',
    link: '/admin/languages'
  }
];
