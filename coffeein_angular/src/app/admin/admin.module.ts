import {NgModule} from '@angular/core';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {NbAuthJWTInterceptor} from '@nebular/auth';
import {AdminRoutingModule} from './admin-routing.module';
import {ThemeModule} from '../@theme/theme.module';
import {
  NbButtonModule,
  NbCardModule, NbCheckboxModule,
  NbDialogModule, NbDialogRef,
  NbDialogService, NbInputModule,
  NbLayoutModule, NbListModule,
  NbMenuModule, NbSelectModule,
  NbSidebarModule
} from '@nebular/theme';
import {AdminComponent} from './admin.component';
import {CreateCompanyComponent} from './companies/create-company/create-company.component';
import {CreateProductComponent} from './products/create-product/create-product.component';
import {CreateCategoryComponent} from './product-categories/create-category/create-category.component';
import {CreateSlideComponent} from './slider-info/create-slide/create-slide.component';
import {UserComponent} from './users/user/user.component';
import {OrdersComponent} from './order-requests/orders/orders.component';
import {LanguageComponent} from './languages/language/language.component';
import {SmartTableComponent} from "./shared/smart-table/smart-table.component";
import {Ng2SmartTableModule} from 'ng2-smart-table';
import {AdminUserProfileService} from "./admin-user-profile.service";
import {TokenInterceptor} from "./admin-token-interceptor";
import {AccessTokenService} from "./consts/access-token.service";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {FileUploadModule} from 'ng2-file-upload';
import {ImageUploadModule} from "angular2-image-upload";
import {QuillModule} from "ngx-quill";
import {QuillComponentComponent} from './shared/quill-component/quill-component.component';

@NgModule({
  imports: [

    HttpClientModule,
    FileUploadModule,
    ImageUploadModule.forRoot(),
    AdminRoutingModule,
    ThemeModule,
    QuillModule.forRoot(),
    NbMenuModule,
    NbSidebarModule.forRoot(),
    NbDialogModule.forRoot(),
    NbLayoutModule,
    NbCardModule,
    NbListModule,
    NbButtonModule,
    NbInputModule,
    Ng2SmartTableModule,
    FormsModule,
    ImageUploadModule,
    NbCheckboxModule,
    NbSelectModule,
    ReactiveFormsModule,
  ],
  declarations: [AdminComponent, SmartTableComponent, CreateCompanyComponent, CreateProductComponent, CreateCategoryComponent, CreateSlideComponent, UserComponent, OrdersComponent, LanguageComponent, QuillComponentComponent],
  providers: [
    {provide: HTTP_INTERCEPTORS, useClass: NbAuthJWTInterceptor, multi: true},
    AdminUserProfileService,
    TokenInterceptor,
    AccessTokenService,
    NbDialogService,
  ]
})


export class AdminModule {
}
