import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {AdminComponent} from './admin.component';
import {FooterComponent} from "../@theme/components";
import {PrincipalPageComponent} from "../principal-page/principal-page.component";
import {CreateCompanyComponent} from "./companies/create-company/create-company.component";
import {CreateCategoryComponent} from "./product-categories/create-category/create-category.component";
import {UserComponent} from "./users/user/user.component";
import {CreateProductComponent} from "./products/create-product/create-product.component";
import {LanguageComponent} from "./languages/language/language.component";
import {CreateSlideComponent} from "./slider-info/create-slide/create-slide.component";
import {OrdersComponent} from "./order-requests/orders/orders.component";


const routes: Routes = [
  {
    path: '',
    component: AdminComponent,
    children: [
      {
        path: 'companies',
        component: CreateCompanyComponent,
       /* loadChildren: () => {
          return import((`./companies/companies.module`)).then(m => m.CompaniesModule);
        }*/
      },
      {
        path: 'users',
        component: UserComponent
        // loadChildren: () => import('./users/users.module').then(m => m.UsersModule)
      },
      {
        path: 'product-categories',
        component: CreateCategoryComponent,
      /*  loadChildren: () => {
          return import((`./product-categories/product-category.module`)).then(m => m.ProductCategoryModule);
        }*/
      },
      {
        path: 'products',
        component: CreateProductComponent
  /*      loadChildren: () => {
          return import((`./products/products.module`)).then(m => m.ProductsModule);
        }*/
      },
      {
        path: 'languages',
        component: LanguageComponent
        // loadChildren: () => import('./languages/languages.module').then(m => m.LanguagesModule)
      },
      {
        path: 'slider-info',
        component: CreateSlideComponent
       /* loadChildren: () => {
          return import((`./slider-info/slider.module`)).then(m => m.SliderModule);
        }*/
      },
      {
        path: 'order-requests',
        component: OrdersComponent
        // loadChildren: () => import('./order-requests/order-requests.module').then(m => m.OrderRequestsModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule {
}
