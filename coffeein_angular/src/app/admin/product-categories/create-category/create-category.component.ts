import {Component, OnInit, TemplateRef} from '@angular/core';
import {NbDialogService} from "@nebular/theme";
import {HttpClient} from "@angular/common/http";
import {AccessTokenService} from "../../consts/access-token.service";
import {ActivatedRoute, Router} from "@angular/router";
import {CookieService} from "ngx-cookie-service";

@Component({
  selector: 'app-create-category',
  templateUrl: './create-category.component.html',
  styleUrls: ['./create-category.component.scss']
})
export class CreateCategoryComponent implements OnInit {

  lg;
  language = 'ro';
  id;
  isEditPage = false;
  object = {
    name: [],
    description: [],
    slug: '',
    range: ''
  };
  columns: {}
  response;
  change_array = [];

  constructor(private dialogService: NbDialogService,
              private http: HttpClient,
              public accessToken: AccessTokenService,
              private route: ActivatedRoute,
              private router: Router,
              private cookieService: CookieService) {
    route.queryParams.subscribe(p => {
      this.id = p.Id;
      if (this.id !== undefined) {
        this.isEditPage = true;
        this.http.get(`http://localhost:3000/api/categories/${this.id}`).subscribe(data => {
          // @ts-ignore
          this.object = data;
        });
      }
    });

  }

  getChangeArray(data) {
    for (let i = 0; i < data.docs.length; i++) {
      this.change_array[i] = {
        id: data.docs[i].id,
        name: data.docs[i].name[this.lg].content,
        description: data.docs[i].description[this.lg].content,
        slug: data.docs[i].slug
      }
    }
    return this.change_array;
  }

  open(dialog: TemplateRef<any>) {
    this.dialogService.open(dialog, {});
    this.router.navigate(
      ['.'],
      {relativeTo: this.route}
    );
    this.object = {
      name: [],
      description: [],
      slug: '',
      range: ''
    };
    this.isEditPage = false;

    this.http.get<any>('http://localhost:3000/api/languages?limit=9999999999999999999').subscribe(data => {
      for (let i = 0; i < data.docs.length; i++) {
        this.object.name.push({shortName: data.docs[i].slug, content: ''});
        this.object.description.push({shortName: data.docs[i].slug, content: ''});
      }
    });
  }

  ngOnInit(): void {
    if (this.cookieService.get('lang') !== '') {
      this.language = this.cookieService.get('lang');
    }
    this.http.get<any>('http://localhost:3000/api/languages?limit=9999999999999999999').subscribe(data => {
      for (let i = 0; i < data.docs.length; i++) {
        if (data.docs[i].slug === this.language) {
          this.lg = i;
        }
      }
    });

    this.http.get('http://localhost:3000/api/categories?limit=9999999999999999999').subscribe(data => {
      this.response = this.getChangeArray(data);
      this.change_array = [];


    });
    this.http.get('http://localhost:3000/api/categories?limit=9999999999999999999').subscribe(data => {
      this.response = this.getChangeArray(data);
      this.change_array = [];
    });


    this.columns = {
      id: {
        title: 'ID'
      },
      name: {
        title: 'NAME'
      },
      description: {
        title: 'Description'
      },
      slug: {
        title: 'Slug'
      },

    }



  }

  onSubmit() {

/*
    for (let i = 0; i < this.languages.length; i++) {
      this.name_languages[i] = {shortName: this.languages[i], content: this.name[i] ? this.name[i] : ''};
      this.description_languages[i] = {
        shortName: this.languages[i],
        content: this.description[i] ? this.description[i] : ''
      };
    }
*/


    if (this.isEditPage) {
      this.http.put(`http://localhost:3000/api/categories/${this.id}`, JSON.stringify(this.object), {headers: this.accessToken.headers}).subscribe(res => {
        this.http.get('http://localhost:3000/api/categories?limit=9999999999999999999').subscribe(data => {
          this.response = this.getChangeArray(data);
          this.change_array = [];


        });
      });
    } else {
      this.http.post(`http://localhost:3000/api/categories`, JSON.stringify(this.object), {headers: this.accessToken.headers}).subscribe(res => {
        this.http.get('http://localhost:3000/api/categories?limit=9999999999999999999').subscribe(data => {
          this.response = this.getChangeArray(data)
          this.change_array = [];

        });
      });
    }


  }

}
