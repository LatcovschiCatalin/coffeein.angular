import {HttpHeaders} from "@angular/common/http";
import {Injectable} from "@angular/core";

@Injectable()
export class AccessTokenService {

  headers = new HttpHeaders({
    'Content-Type': 'application/json',
    'Authorization': `Bearer ${JSON.parse(localStorage.getItem('auth_app_token')).value}`
  })
  headers2 = new HttpHeaders({
    'Authorization': `Bearer ${JSON.parse(localStorage.getItem('auth_app_token')).value}`
  })

}

