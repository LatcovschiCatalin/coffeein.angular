import {Component} from '@angular/core';

import {MENU_ITEMS} from './admin-menu';
import {AuthService} from "../auth/auth.service";

@Component({
  selector: 'ngx-pages',
  styleUrls: ['admin.component.scss'],
  templateUrl: './admin.component.html',
})
export class AdminComponent {
  change = window.location.href.split('/');
  select;

  constructor(private authService: AuthService) {
    for (let i = 0; i < this.change.length; i++) {

      if (this.change[i] === 'admin' && this.change.length - 1 === i + 1) {
        this.select = this.change[i + 1].split('?')[0];
        break;
      } else {
        this.select = '';
      }
    }
  }

  selected(item) {
    this.select = item;
  }

  logout() {
    return this.authService.logout();
  }

  menu = MENU_ITEMS;
}
