import {Component, OnInit, TemplateRef} from '@angular/core';
import {NbDialogService} from "@nebular/theme";
import {HttpClient} from "@angular/common/http";
import {AccessTokenService} from "../../consts/access-token.service";
import {ActivatedRoute, Router} from "@angular/router";
import {CookieService} from "ngx-cookie-service";

@Component({
  selector: 'app-create-product',
  templateUrl: './create-product.component.html',
  styleUrls: ['./create-product.component.scss']
})
export class CreateProductComponent implements OnInit {
  lg;
  selectCategory;
  selectCompany;
  language = 'ro';
  id;
  company_name;
  category_name;
  companies = [];
  categories = [];
  isEditPage = false;
  object = {
    title: [],
    description: [],
    mainDescription: [],
    slug: '',
    order: '0',
    price: '',
    category: {},
    company: {},
    image: {}
  };

  columns: {}
  response;
  pages;
  change_array = [];
  image_src = {};
  imageUpload;
  editImage = [];

  constructor(private dialogService: NbDialogService,
              private http: HttpClient,
              public accessToken: AccessTokenService,
              private route: ActivatedRoute,
              private router: Router,
              private cookieService: CookieService) {
    route.queryParams.subscribe(p => {
      this.id = p.Id;
      this.editImage = []

      if (this.id !== undefined) {
        this.isEditPage = true;
        this.http.get(`http://localhost:3000/api/products/${this.id}`).subscribe(data => {
          // @ts-ignore
          this.object = data;
          // @ts-ignore
          this.selectCategory = data.category.name[0].content;
          // @ts-ignore
          this.selectCompany = data.company.name;

          // @ts-ignore
          if (data.image && data.image.path) {
            // @ts-ignore
            this.image_src = data.image;
            // @ts-ignore
            this.editImage = [{url: 'http://localhost:3000/api/' + data.image.path.replaceAll('\\', '/'), fileName: data.image.description
            }];

          }
        });
      }
    });

  }

  getChangeArray(data) {
    for (let i = 0; i < data.docs.length; i++) {
      this.change_array[i] = {
        id: data.docs[i].id,
        title: data.docs[i].title[this.lg].content,
        description: data.docs[i].description[this.lg].content,
        mainDescription: data.docs[i].mainDescription[this.lg].content,
        slug: data.docs[i].slug
      }
    }
    return this.change_array;
  }

  open(dialog: TemplateRef<any>) {
    this.editImage = []
    this.image_src = [];
    this.dialogService.open(dialog, {});
    this.router.navigate(
      ['.'],
      {relativeTo: this.route}
    );
    this.object = {
      title: [],
      description: [],
      mainDescription: [],
      slug: '',
      order: '0',
      price: '',
      category: {},
      company: {},
      image: {}
    };

    this.isEditPage = false;
    // @ts-ignore
    this.selectCategory = '';
    // @ts-ignore
    this.selectCompany = '';

    this.http.get<any>('http://localhost:3000/api/languages?limit=9999999999999999999').subscribe(data => {
      for (let i = 0; i < data.docs.length; i++) {
        this.object.title.push({shortName: data.docs[i].slug, content: ''});
        this.object.description.push({shortName: data.docs[i].slug, content: ''});
        this.object.mainDescription.push({shortName: data.docs[i].slug, content: ''});
      }
    });
  }

  ngOnInit(): void {
    this.imageUpload = 'http://localhost:3000/api/upload/product/image';


    this.http.get('http://localhost:3000/api/companies?limit=9999999999999999999').subscribe(data => {
      // @ts-ignore
      this.companies = data.docs;
    });
    this.http.get('http://localhost:3000/api/categories?limit=9999999999999999999').subscribe(data => {
      // @ts-ignore
      this.categories = data.docs;
    });

    if (this.cookieService.get('lang') !== '') {
      this.language = this.cookieService.get('lang');
    }
    this.http.get<any>('http://localhost:3000/api/languages?limit=9999999999999999999').subscribe(data => {
      for (let i = 0; i < data.docs.length; i++) {
        if (data.docs[i].slug === this.language) {
          this.lg = i;
        }
      }
    });

    this.http.get('http://localhost:3000/api/products?limit=9999999999999999999').subscribe(data => {
      this.response = this.getChangeArray(data);
      this.change_array = [];


    });



    this.columns = {
      id: {
        title: 'ID'
      },
      title: {
        title: 'Title'
      },
      description: {
        title: 'Description'
      },
      mainDescription: {
        title: 'Main Description'
      },
      slug: {
        title: 'Slug'
      },

    }


  }

  setProductCompany(event) {
    this.http.get('http://localhost:3000/api/companies?limit=9999999999999999999').subscribe(data => {
      // @ts-ignore
      for (let i = 0; i < data.docs.length; i++) {
        // @ts-ignore

        if (data.docs[i].name === event) {
          // @ts-ignore

          this.company_name = data.docs[i];
        }
      }
    });
  }

  setCategory(event) {
    this.http.get('http://localhost:3000/api/categories?limit=9999999999999999999').subscribe(data => {
      // @ts-ignore

      for (let i = 0; i < data.docs.length; i++) {
        // @ts-ignore

        if (data.docs[i].name[0].content === event) {
          // @ts-ignore

          this.category_name = data.docs[i];
          console.log('category', this.category_name);
        }
      }
    });
  }

  onSubmit() {

    this.object.image = this.image_src;


    this.object.company = this.company_name;
    this.object.category = this.category_name;

    if (this.isEditPage) {
      this.http.put(`http://localhost:3000/api/products/${this.id}`, JSON.stringify(this.object), {headers: this.accessToken.headers}).subscribe(res => {
        this.http.get('http://localhost:3000/api/products?limit=9999999999999999999').subscribe(data => {
          this.response = this.getChangeArray(data);
          this.change_array = [];

        });

        this.http.get(`http://localhost:3000/api/products/${this.id}`).subscribe(data => {
          // @ts-ignore
          this.editImage = [{url: 'http://localhost:3000/api/' + data.image.path.replaceAll('\\', '/'), fileName: data.image.description
          }];

        });
      });
    } else {
      this.http.post(`http://localhost:3000/api/products`, JSON.stringify(this.object), {headers: this.accessToken.headers}).subscribe(res => {
        this.http.get('http://localhost:3000/api/products?limit=9999999999999999999').subscribe(data => {
          this.response = this.getChangeArray(data)
          this.change_array = [];

        });
      });
    }
  }


  uploadFinished(event) {
    if (event && event.serverResponse && event.serverResponse.response && event.serverResponse.response.body) {
      this.image_src = {
        path: event.serverResponse.response.body.path.replaceAll('\\', '/'),
        mimetype: event.serverResponse.response.body.mimetype,
        description: event.serverResponse.response.body.filename
      };
    }

  }

  removeImage() {
    this.image_src = [];
  }

}
