import {Component, OnInit} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Router} from "@angular/router";

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  company_slug;
  color;
  map_data;
  latitude;
  longitude;
  company_logo;

  constructor(private http: HttpClient,
              private route: Router) {
  }

  ngOnInit(): void {
    this.company_slug = this.route.url.split('/');

    this.http.get<any>('http://localhost:3000/api/companies').subscribe(company => {
      for (let i = 0; i < company.docs.length; i++) {

        if (company.docs[i].slug === this.company_slug[2]) {
          this.map_data = company.docs[i].coordinates.split(',');
          this.company_logo = company.docs[i];
          this.latitude = this.map_data[1];
          this.longitude = this.map_data[2];

        }
      }

    });

  }

}
