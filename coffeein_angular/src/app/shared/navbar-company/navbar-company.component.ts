import {Component, Input, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {HttpClient} from "@angular/common/http";

@Component({
  selector: 'app-navbar-company',
  templateUrl: './navbar-company.component.html',
  styleUrls: ['./navbar-company.component.scss']
})
export class NavbarCompanyComponent implements OnInit {
  @Input() company_name;
  @Input() way;
  @Input() value_language;

  company_slug;
  company_logo;


  constructor(private router: Router,
              private http: HttpClient) {
  }

  getCompanyProducts(way) {
    if (way === '') {
      this.router.navigate(['']);
    } else {
      this.router.navigate(['/' + way, this.company_slug[2]]);
    }
  }

  ngOnInit(): void {
    this.company_slug = this.router.url.split('/');
    this.http.get<any>('http://localhost:3000/api/companies').subscribe(company => {
      for (let i = 0; i < company.docs.length; i++) {
        if (company.docs[i].slug === this.company_slug[2]) {
          this.company_logo = company.docs[i];


        }
      }
    });
  }

}
