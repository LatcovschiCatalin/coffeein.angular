import {Component, HostBinding, Input, OnInit, TemplateRef} from '@angular/core';
import {NbDialogService, NbToastrService} from '@nebular/theme';
import {HttpClient} from '@angular/common/http';
import * as Leaflet from "leaflet";
import {Router} from "@angular/router";
import {CookieService} from "ngx-cookie-service";
import {AccessTokenService} from "../../admin/consts/access-token.service";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-send-popup',
  templateUrl: './send-popup.component.html',
  styleUrls: ['./send-popup.component.scss']
})
export class SendPopupComponent implements OnInit {
  registerForm: FormGroup;
  language = 'ro';
  phone_number;
  submitted = false;
  full_name;
  data;
  @Input() price;
  @Input() value_language;
  date = new Date().toISOString();

  checked = false;
  title = 'leafletApps';
  map: Leaflet.Map;
  company_slug;
  map_data;
  lat;
  long;
  share_location;
  latitude;
  products;
  longitude;
  all_products = [];


  toggle(checked: boolean) {
    this.checked = checked;
  }

  @HostBinding('class')
  classes = 'example-items-rows';

  constructor(public dialogService: NbDialogService,
              private http: HttpClient,
              private route: Router,
              private toastrService: NbToastrService,
              private cookieService: CookieService,
              public accessToken: AccessTokenService,
              private formBuilder: FormBuilder
  ) {
  }


  showToast(position, status) {
    this.submitted = true;


    this.products = JSON.parse(this.cookieService.get('cart-products'));

    this.all_products = [];

    this.products.map(el => {
      this.http.get(`http://localhost:3000/api/products/${el.id}`).subscribe(res => {
        // @ts-ignore
        this.all_products.push({...res, cartCount: el.count, cookiePrice: Number(res.price)});
        this.data =
          {
            phone: this.registerForm.value.phone,
            totalPrice: this.price,
            address: this.share_location ?
              `https://www.google.com/maps/search/${this.latitude},+${this.longitude}` : 'No address indicated',
            fullName: this.registerForm.value.fullName && this.registerForm.value.fullName !== '' ? this.registerForm.value.fullName : 'No name indicated',
            products: this.all_products
          }

        if (el.id === this.products[this.products.length - 1].id) {
          if (this.registerForm.invalid) {
            return;
          } else {
            this.http.post<any>('http://localhost:3000/api/order-request', this.data, {headers: this.accessToken.headers}).subscribe(res => {
            });

          }
        }

      });
    })

    this.cookieService.deleteAll();



    this.toastrService.show(
      status || 'success',
      `Comanda a fost trimisa cu succes!`,
      {position, status});
  }


  open(dialog: TemplateRef<any>) {
    this.dialogService.open(dialog, {});
    this.share_location = false;


  }

  getLocation(): void {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.longitude = Number(position.coords.longitude);
        this.latitude = Number(position.coords.latitude);
        this.share_location = true;
      });
    } else {
      console.log("No support for geolocation")
    }
  }

  get f() {
    return this.registerForm.controls;
  }

  ngOnInit(): void {
    if (this.cookieService.get('lang') !== '') {
      this.language = this.cookieService.get('lang');
    }

    this.registerForm = this.formBuilder.group({
      phone: ['', [Validators.required]],
      fullName: ['']
    });


    this.company_slug = this.route.url.split('/');

    this.http.get<any>('http://localhost:3000/api/companies').subscribe(company => {
      for (let i = 0; i < company.docs.length; i++) {

        if (company.docs[i].slug === this.company_slug[2]) {
          this.map_data = company.docs[i].coordinates.split(',');
          this.lat = this.map_data[1];
          this.long = this.map_data[2];

        }
      }
    });
    this.http.get(`../assets/languages/${this.language}.json`).subscribe(data => {

      this.value_language = data;

    });

  }


}
