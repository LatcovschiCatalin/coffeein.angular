import {Component, HostListener} from '@angular/core';
import {process} from "@angular/compiler-cli/ngcc";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],

})

export class AppComponent {
  title = 'coffeein-angular';
  isTop;

  getTopPage() {
    window.scroll({
      top: 0,
      left: 0,
      behavior: 'smooth',
    });
  }
  @HostListener('window:scroll')
  checkScroll() {
    const scrollPosition = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop || 0;

    this.isTop = Number(scrollPosition) >= window.innerHeight;
  }



}
