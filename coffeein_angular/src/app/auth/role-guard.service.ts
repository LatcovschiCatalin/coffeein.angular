import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router} from '@angular/router';
import {AuthService} from './auth.service';

import {NbTokenService} from '@nebular/auth';
import {map} from 'rxjs/operators';
import {Observable} from 'rxjs';

@Injectable()
export class RoleGuard implements CanActivate {


  constructor(public auth: AuthService, public router: Router, private NbJwt: NbTokenService) {
  }

  canActivate(route: ActivatedRouteSnapshot): Observable<boolean> {
    const expectedRole = route.data.expectedRole;
    return this.NbJwt.get().pipe(
      map(token => {
        const tokenPayload = token.getPayload();
        if (
          tokenPayload.roles.includes(expectedRole)
        ) {
          localStorage.removeItem('ami_auth_token');
          this.router.navigate(['auth/login']);
          return false;
        }
        return true;
      })
    );
  }
}
